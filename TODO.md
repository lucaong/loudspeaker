# TODO

  - [ ] refactor strategies to avoid duplication
  - [x] endpoint to subscribe to multiple channels at once
  - [ ] AJAX long polling strategy
  - [x] JSON response in AJAX strategies
  - [x] JavaScript client
  - [ ] tests
  - [ ] close channel
