window.Loudspeaker = function( host ) {
  host = host || ""
  var padding = 2048

  var hasEventSource = function() {
    return !!window.EventSource
  }

  var isArray = function( maybeArray ) {
    return Object.prototype.toString.call( maybeArray ) === "[object Array]"
  }

  var contains = function( array, element ) {
    return array.indexOf( element ) >= 0
  }

  var query = function( channels ) {
    return "?" + channels.map(function( c ) { return "name=" + c }).join("&")
  }

  var subscribeWithSSE = function( channels, handler ) {
    var source = new EventSource( host + "/channels" + query( channels ) )
    var wrapper = function( evt ) {
      handler({ payload: evt.data, channel: evt.type, timestamp: evt.lastEventId })
    }
    channels.forEach( function( channel ) {
      source.addEventListener( channel, wrapper, false )
    })
  }

  var subscribeWithAJAXStreaming = function( channels, handler ) {
    var offset
    var lastEventID

    var handleChunk = function( evt ) {
      var length = this.responseText.length
      if ( length > padding ) {
        var blob = this.responseText.substr( offset )
        offset = length
        if ( blob === " " ) return
        var data = JSON.parse( blob )
        if ( isArray( data ) ) {
          data.forEach(function( d ) {
            lastEventID = d.timestamp
            if ( contains( channels, d.channel ) ) {
              handler( d )
            }
          })
        } else {
          lastEventID = data.timestamp
          if ( contains( channels, data.channel ) ) {
            handler( data )
          }
        }
      }
    }

    var perform = function() {
      var xhr = new XMLHttpRequest();
      offset = padding
      xhr.addEventListener( "progress", handleChunk, false )
      xhr.addEventListener( "error", retry, false )
      xhr.open( "GET", host + "/channels" + query( channels ) )
      if ( lastEventID ) {
        xhr.setRequestHeader( "Last-Event-ID", lastEventID )
      }
      xhr.send()
    }

    var retry = function() {
      setTimeout( perform, 3000 )
    }

    perform()
  }

  var subscribe = function( channels, handler ) {
    if ( !isArray( channels ) ) {
      channels = [ channels ]
    }

    if ( hasEventSource() ) {
      subscribeWithSSE( channels, handler )
    } else {
      subscribeWithAJAXStreaming( channels, handler )
    }
  }

  return {
    subscribe: subscribe
  }
}
