package com.lucaongaro.loudspeaker

import akka.actor._
import akka.event.EventBus
import akka.event.LookupClassification

class ChannelBus extends EventBus with LookupClassification {
  type Subscriber = ActorRef
  type Classifier = String
  type Event      = Message

  override protected def classify(
    event: Event
  ): Classifier = event.channel

  override protected def publish(
    event:      Event,
    subscriber: Subscriber
  ): Unit = subscriber ! event

  override protected def compareSubscribers(
    a: Subscriber,
    b: Subscriber
  ): Int = a compareTo b

  override protected def mapSize = 256
}
