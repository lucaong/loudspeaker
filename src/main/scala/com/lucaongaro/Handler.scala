package com.lucaongaro.loudspeaker

import akka.actor._
import spray.can.Http
import spray.routing._
import spray.http._
import spray.http.StatusCodes._
import strategies._

class Handler extends HttpServiceActor with ActorLogging {

  def receive = runRoute {
    get {
      // GET /ping - Ping the server to check if it is alive
      path("ping") {
        complete("pong")
      } ~
      optionalHeaderValueByName("Last-Event-ID") { lastId =>
        optionalHeaderValueByName("Accept") { accept =>
          val lastTimestamp = lastId.map( _.toLong )
          val strategy      = selectStrategy( accept )

          // GET /channels?name=foo&name=bar - Subscribe to several channels
          path("channels") {
            parameterMultiMap { params => requestContext =>
              params.get("name") match {
                case None =>
                  failWith( new IllegalRequestException( NotAcceptable, "no channel name specified" ) )
                case Some( channels ) =>
                  subscribe( channels, requestContext.responder, strategy, lastTimestamp )
              }
            }
          } ~
          // GET /channels/:name - Subscribe to one channel
          path("channels" / """(\w+)""".r) { channel => requestContext =>
            subscribe( List( channel ), requestContext.responder, strategy, lastTimestamp )
          }
        }
      }
    } ~
    post {
      // POST /channels/:name - Publish on a channel
      path("channels" / """(\w+)""".r) { channel =>
        entity(as[String]) { payload =>
          val message = Message( channel, payload )
          Loudspeaker.channelBus.publish( message )
          Loudspeaker.messageCache.put( message )
          complete("OK")
        }
      }
    }
  }

  def subscriber( strategy: Strategy, responder: ActorRef ): Actor.Receive = {
    case message @ Message( channel, data, timestamp ) =>
      strategy.send( responder, message )

    case _: Http.ConnectionClosed =>
      Loudspeaker.channelBus.unsubscribe( self )
      context.stop( self )
  }

  private def subscribe(
    channels:      List[String],
    responder:     ActorRef,
    strategy:      Strategy,
    lastTimestamp: Option[Long]
  ) = {
    strategy.sendStart( responder, channels, lastTimestamp )
    context become subscriber( strategy, responder )
    channels.foreach { channel =>
      Loudspeaker.channelBus.subscribe( self, channel )
    }
  }

  private def selectStrategy(
    accept: Option[String]
  ): Strategy = {
    accept match {
      case Some("text/event-stream") => ServerSentEvents
      case _                         => AJAXStreaming
    }
  }
}
