package com.lucaongaro.loudspeaker

import akka.actor._
import spray.can.Http

class Listener extends Actor with ActorLogging {
  def receive = {
    case _: Http.Connected =>
      sender ! Http.Register( context.actorOf( Props[Handler] ) )
  }
}
