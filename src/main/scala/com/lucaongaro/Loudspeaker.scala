package com.lucaongaro.loudspeaker

import akka.actor.{ ActorSystem, Props }
import akka.io.IO
import spray.can.Http

object Loudspeaker extends App {
  implicit val system = ActorSystem()

  val listener     = system.actorOf( Props[Listener], name = "listener" )
  val channelBus   = new ChannelBus()
  val messageCache = new MessageCache()

  IO(Http) ! Http.Bind( listener, interface = "localhost", port = 8888 )
}
