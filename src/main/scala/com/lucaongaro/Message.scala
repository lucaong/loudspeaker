package com.lucaongaro.loudspeaker

import spray.http._
import spray.json._
import scala.collection.immutable.SortedSet

case class Message(
  channel:   String,
  payload:   String,
  timestamp: Long = System.currentTimeMillis
)

object Message {
  /** Default ordering by ascending timestamp */
  implicit val defaultOrdering = Ordering.fromLessThan[Message] {
    _.timestamp < _.timestamp
  }

  def dummyAt(
    timestamp: Long
  ) = Message( "2ySDrSpLUVpq6icM5LiYmw", "", timestamp )
}

/** JSON serialization of Message */
object MessageJsonProtocol extends DefaultJsonProtocol {
  implicit val MessageJsonFormat = jsonFormat3( Message.apply )

  implicit object MessagesJsonFormat extends RootJsonFormat[SortedSet[Message]] {
    def write( messages: SortedSet[Message] ) = {
      new JsArray( messages.toList.map( _.toJson ) )
    }

    def read( value: JsValue ) = {
      value match {
        case jsArray: JsArray =>
          jsArray.elements.foldLeft( SortedSet.empty[Message] ) { ( set, json ) =>
            set + json.convertTo[Message]
          }
        case _ =>
          throw new DeserializationException("Messages expected")
      }
    }
  }
}
