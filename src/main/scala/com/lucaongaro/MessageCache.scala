package com.lucaongaro.loudspeaker

import com.google.common.cache.{Cache => GuavaCache, CacheBuilder}
import scala.collection.immutable.SortedSet
import java.util.concurrent.TimeUnit

class MessageCache(
  maxSize:     Int = 1000,  
  expireAfter: Int = 60 // 1 minute
) {
  private val cache: GuavaCache[String, SortedSet[Message]] = CacheBuilder.newBuilder()
    .maximumSize( maxSize )
    .expireAfterWrite( expireAfter, TimeUnit.SECONDS )
    .build()

  def getAfter(
    channel:   String,
    timestamp: Long
  ): SortedSet[Message] = {
    cache.getIfPresent( channel ) match {
      case null => SortedSet.empty[Message]
      case set  => set.from( Message.dummyAt( timestamp ) )
    }
  }

  def getAfter(
    channels:  Seq[String],
    timestamp: Long
  ): SortedSet[Message] = {
    channels.foldLeft( SortedSet.empty[Message] ) { ( messages, channel ) =>
      messages ++ getAfter( channel, timestamp )
    }
  }

  def put(
    message: Message
  ) {
    cache.put( message.channel, getAfter( message.channel, expireTimeAgo ) + message )
  }

  private def expireTimeAgo = System.currentTimeMillis - expireAfter * 1000
}
