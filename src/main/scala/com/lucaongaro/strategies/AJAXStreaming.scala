package com.lucaongaro.loudspeaker.strategies

import akka.actor._
import spray.http._
import spray.http.MediaTypes._
import spray.http.HttpHeaders._
import spray.http.CacheDirectives._
import spray.json._
import DefaultJsonProtocol._
import com.lucaongaro.loudspeaker._
import MessageJsonProtocol._

object AJAXStreaming extends Strategy {

  def sendStart(
    recipient:     ActorRef,
    channels:      Seq[String],
    lastTimestamp: Option[Long]
  ) = {
    val payload = lastTimestamp match {
      case Some( time ) => padding + messagesAfter( channels, time ).toJson.toString
      case None         => padding + Message.dummyAt( System.currentTimeMillis ).toJson.toString
    }
    val responseHeaders = List( `Cache-Control`(`no-cache`), `Access-Control-Allow-Origin`(AllOrigins) )
    val responseStart   = HttpResponse(
      entity  = HttpEntity( `text/plain`, payload ),
      headers = responseHeaders
    )
    recipient ! ChunkedResponseStart( responseStart )
    // Apparently Chrome wants a chunk or it does not render the response text
    recipient ! MessageChunk(" ")
  }

  def send( recipient: ActorRef, message: Message ) =
    recipient ! MessageChunk( message.toJson.toString )

  // Pad ~2048 bytes of spaces to ensure response rendering
  private val padding = " " * 2048

  private def messagesAfter( channels: Seq[String], timestamp: Long ) = {
    Loudspeaker.messageCache.getAfter( channels, timestamp )
  }
}
