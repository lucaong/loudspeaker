package com.lucaongaro.loudspeaker.strategies

import akka.actor._
import spray.http._
import spray.http.MediaTypes._
import spray.http.HttpHeaders._
import spray.http.CacheDirectives._
import com.lucaongaro.loudspeaker._


object ServerSentEvents extends Strategy {
  val `text/event-stream` = register( MediaType.custom("text/event-stream") )

  def sendStart(
    recipient:     ActorRef,
    channels:      Seq[String],
    lastTimestamp: Option[Long]
  ) = {
    val payload = lastTimestamp match {
      case Some( time ) => padding + wrapMessagesAfter( channels, time )
      case None         => padding + wrap( Message.dummyAt( System.currentTimeMillis ) )
    }
    val responseHeaders = List( `Cache-Control`(`no-cache`), `Access-Control-Allow-Origin`(AllOrigins) )
    val responseStart   = HttpResponse(
      entity  = HttpEntity( `text/event-stream`, payload ),
      headers = responseHeaders
    )
    recipient ! ChunkedResponseStart( responseStart )
  }

  def send( recipient: ActorRef, message: Message ) = {
    recipient ! MessageChunk( wrap( message ) )
  }

  // Pad ~2048 bytes of newlines to ensure response rendering
  private val padding = "\n" * 2048

  private def wrapMessagesAfter( channels: Seq[String], timestamp: Long ) = {
    Loudspeaker.messageCache.getAfter( channels, timestamp )
      .foldLeft("") { _ + wrap( _ ) }
  }

  private def wrap( message: Message ) = {
    val lines = message.payload.replaceAll("""\n""", "\ndata: ")
    s"id: ${message.timestamp}\nevent: ${message.channel}\ndata: $lines\n\n"
  }
}
