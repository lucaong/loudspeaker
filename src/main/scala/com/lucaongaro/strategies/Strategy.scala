package com.lucaongaro.loudspeaker.strategies

import akka.actor.ActorRef
import com.lucaongaro.loudspeaker._

trait Strategy {
  def sendStart(
    recipient:     ActorRef,
    channels:      Seq[String],
    lastTimestamp: Option[Long]
  ): Unit

  def send(
    recipient: ActorRef,
    message:   Message
  ): Unit
}
